<?php

namespace App\Commands;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Support\Facades\File;
use LaravelZero\Framework\Commands\Command;

class Install extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'install';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Installs the framework';

    private $installation_path;

    private $git_user_name;

    private $git_access_token;

    private $asked_env_vars = [];

    private $available_modules = [];

    private $selected_modules_to_install = [];

    private $apache_user_name;

    private $system_user_name;

    public function __construct()
    {
        parent::__construct();
        $this->available_modules = config('module_repo_detail');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
        $this->table(['Welcome to the framework installation wizard'], [['You will be guided through the installation steps']]);

        $this->checkIfGitIsInstalled();

        $this->askInstallationPath();

        $this->checkIfInstallationPathIsAlreadyOccupied();

        $this->askGitUserName();

        $this->askGitAccessToken();

        $this->askEnvVars();

        $this->selectModulesToInstall();

        $this->fetchMainFrameworkSkeleton();

        $this->installSelectedModules();

        $this->setUpEnvironment();

        $this->performFrameworkKitUpdate();

        $this->fixPermissions();
    }

    /**
     * @throws \Exception
     */
    public function checkIfGitIsInstalled()
    {
        $this->info('Checking if Git is installed');
        exec('git --version', $output, $status_code);
        if ($status_code !== 0) {
            throw new \Exception('Git is not installed');
        }
        $this->info('Git is installed and working fine');
    }

    public function askInstallationPath()
    {
        $this->installation_path = $this->ask('Please specify the full path for installation');
    }

    public function checkIfInstallationPathIsAlreadyOccupied()
    {
        if (is_dir($this->installation_path)) {
            $this->info($this->installation_path . ' is already a directory');
            $choice = $this->choice('Are you sure to install on this path?', [
                'yes' => 'yes',
                'no' => 'no',
            ], 'no');
            if ($choice == 'no') {
                $this->askInstallationPath();
                $this->checkIfInstallationPathIsAlreadyOccupied();
            }
        }
    }

    /**
     * @throws \Exception
     */
    public function fetchMainFrameworkSkeleton()
    {
        $url_to_download_from = 'https://' . $this->git_user_name . ':' . $this->git_access_token . '@gitlab.com/rakshitafaz/framework.git';

        if (file_exists($this->installation_path)) {
            $this->removeDirectory($this->installation_path);
        }
        mkdir($this->installation_path, 0755);
        chdir($this->installation_path);
        exec("git clone $url_to_download_from $this->installation_path", $output, $response_code);
        if ($response_code != 0) {
            throw new \Exception('Invalid credentials provided');
        }
    }

    public function askGitUserName()
    {
        $this->git_user_name = $this->ask('Please provide git user name');
    }

    public function askGitAccessToken()
    {
        $this->git_access_token = $this->ask('Please provide git access token');
    }

    public function selectModulesToInstall()
    {
        $module_options = array_keys($this->available_modules);
        sort($module_options);
        $this->selected_modules_to_install = $this->select(
            'Select which modules you want to install',
            $module_options
        );
    }

    public function installSelectedModules()
    {
        $this->info('Installing selected modules');
        if (!empty($this->selected_modules_to_install)) {
            foreach ($this->selected_modules_to_install as $selected_module) {
                $this->installModuleByKey($selected_module);
            }
        }
    }

    public function installModuleByKey($selected_module_key)
    {
        $modules_directory = $this->installation_path . DIRECTORY_SEPARATOR . 'Modules';
        if (isset($this->available_modules[$selected_module_key])) {
            $module_config = $this->available_modules[$selected_module_key];
            if (isset($module_config['git_namespace'])) {
                $modules_installation_directory = $modules_directory . DIRECTORY_SEPARATOR . $module_config['module_folder_name'];
                $url_to_download_from = 'https://' . $this->git_user_name . ':' . $this->git_access_token . '@gitlab.com/' . $module_config['git_namespace'] . '.git';
                exec("git clone $url_to_download_from $modules_installation_directory", $output, $response_code);
                if ($response_code != 0) {
                    $this->warn("$selected_module_key not installed for some reason");
                }
            }
        }
    }

    public function copyDirectory($source, $destination)
    {
        mkdir($destination, 0755);
        foreach (
            $iterator = new \RecursiveIteratorIterator(
                new \RecursiveDirectoryIterator($source, \RecursiveDirectoryIterator::SKIP_DOTS),
                \RecursiveIteratorIterator::SELF_FIRST) as $item
        ) {
            if ($item->isDir()) {
                mkdir($destination . DIRECTORY_SEPARATOR . $iterator->getSubPathName());
            } else {
                copy($item, $destination . DIRECTORY_SEPARATOR . $iterator->getSubPathName());
            }
        }
    }

    public function performFrameworkKitUpdate()
    {
        chdir($this->installation_path);
        $this->info('Performing Kit Update! Sit back and relax');
        exec('sh kit-install.sh', $output, $response_code);
    }

    public function fixPermissions()
    {
        $this->apache_user_name = $this->ask('Specify Apache User Name', 'apache');
        $this->system_user_name = $this->ask('Specify System User Name', '$USER');
        chdir($this->installation_path);
        $this->info('Applying Permissions');
        exec("sh update-file-permissions.sh $this->apache_user_name $this->system_user_name", $output, $response_code);
        if ($response_code != 0) {
            $this->warn('Something went wrong when applying permissions.');
        }
    }

    public function setUpEnvironment()
    {
        if (!empty($this->asked_env_vars)) {
            chdir($this->installation_path);
            $env_file = File::get('.env.example');
            $env_file = explode("\n", $env_file);
            foreach ($this->asked_env_vars as $asked_env_var_key => $asked_env_var_value) {
                foreach ($env_file as $key => &$env_item) {
                    $env_variable = explode('=', $env_item);
                    if ($env_variable[0] == $asked_env_var_key) {
                        $env_variable[1] = $asked_env_var_value;
                        if (strstr($asked_env_var_value, ' ')) {
                            $env_variable[1] = '"' . $asked_env_var_value . '"';
                        }
                    }
                    $env_item = implode('=', $env_variable);
                }
            }
            $env_file = implode("\n", $env_file);
            File::put('.env', $env_file);
        }
    }

    public function askEnvVars()
    {
        $this->asked_env_vars['APP_NAME'] = $this->ask('Add Proper App name to this project');
        $this->asked_env_vars['DB_DATABASE'] = $this->ask('Please enter database name:');
        $this->asked_env_vars['DB_USERNAME'] = $this->ask('Please enter database user name:');
        $this->asked_env_vars['DB_PASSWORD'] = $this->ask('Please enter database password:');
    }

    public function removeDirectory(string $directory)
    {
        $it = new \RecursiveDirectoryIterator($directory, \RecursiveDirectoryIterator::SKIP_DOTS);
        $files = new \RecursiveIteratorIterator($it,
            \RecursiveIteratorIterator::CHILD_FIRST);
        foreach ($files as $file) {
            if ($file->isDir()) {
                rmdir($file->getRealPath());
            } else {
                unlink($file->getRealPath());
            }
        }
        rmdir($directory);
    }

    /**
     * Define the command's schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    public function schedule(Schedule $schedule): void
    {
        // $schedule->command(static::class)->everyMinute();
    }
}
